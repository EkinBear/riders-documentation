# Riders User Guide Documentation

Documentation for [riders.ai](http://riders.ai).

## Launching a Project

* Users are able to access  personal projects at **Projects** tab located at the top-right corner of the page.
* Projects in groups in which user has joined can be accessed under **My Projects** tab at the left side.

![](images/projects_page.png)

* Team projects are visible at **Team Projects** tab at **Group** page.

![](images/group_page.png)

* Information about the project can be found on the **Project** page.
* Information includes branches and users that created the branches.

![](images/project_page.png)

* Clicking edit button on the project page opens the following pop-up.
* Clicking on the **Continue Editing** on the pop-up directs to **Editor**

![](images/edit_project_popup.png)

* If a container is already opened page opens the following pop-up.

* Clicking on the URL on the pop-up directs to **Editor**

  ![](images/container_already_openned_popup.png)

## Editing a Project

* On the left side of the editor screen is the explorer screen and shows the files in the workspace.

![](images/editor_page.png)

* In order to access command line press **New Terminal** tab at the top left corner of the screen and a command line will appear on the bottom of editor.

* Terminal opens a shell script to access bash type bash in the terminal.

  ![](images/terminal.png)

* Clicking on the **Riders** icon reveals actions available.

  

![](images/editor_riders_actions.png)

### Configuration

* Clicking on the **Edit configuration** reveals configuration of current project.

  ![](images/edit_configuration_tab.png)

* **Requirements** consists of three elements and a git repository's URL are given in each one of them.
* **environment** includes gazebo world files and required models for the simulation.
* **robots** are a catkin workspace with robot description defined as as a ros package.
* **packages** are ros packages simulation will need.

* These requirements are installed and built while opening the editor.

  ![](images/edit_config_requirements.png)

* Installed repositories are stored under the lib directory at workspace.

![](images/installed_libs.png)

* Under the **resources** such as robots and simulation worlds are defined.

  ![](images/edit_config_resources.png)

* Under the **runtime** simulation arguments are defined and when **Start Simulation** is clicked launches simulation with given arguments.

* **world** is the environment which gazebo loads.

* Under the **robots** tab models which will be spawned in the simulation defined.

* In this case a robot named **1** which is model1(predefined under the resources) will be spawned at given x,y,z locations and R(Roll), P(Pitch), Y(Yaw) rotations.

* Under the **nodes** given ros node with ros package will run. Defined name will be node's name when launched.

  ![](images/edit_config_runtime.png)

* After changing configuration clicking on **Build Project** installs requirements and builds packages.

  ![](images/editor_riders_actions.png)

### Starting Simulation

* Clicking on **Start Simulation** button starts simulation with defined riders-config.yml file.

![](images/editor_riders_actions.png)

* When simulation is ready gazebo simulation will appear.

  ![](images/simulation_screen.png)

* Clicking on the **Riders Icon** when a simulation is active will reveal more actions.

  ![](images/stop_simulation.png)

* Clicking on **view simulation** re-opens simulation screen.

* Clicking on the **Open Ros Camera** tab will reveal ros image topics with topic name.

  ![](images/ros_camera.png)

* Users are able to see camera images published under selected topic.

  ![](images/active_camera.png)

* Clicking on **Stop Simulation** stops simulation.
* Clicking on **Reset Simulation** re-opens simulation.
* Clicking on the **Stop Container** closes editor.

### User Written Packages

* Users can create their packages under the **src/project** directory and changes will be pushed to project branch.

![](images/src_project.png)

* Users can run their projects via terminal.

  ![](images/zephyr_screen.png)

* For **Havelsan Zephyr Demo** after the simulation is started launching keyboard_controller.py will give control access to user.

* In order to launch node type commands in bash given bellow.

  ```
  $ source devel/setup.bash
  $ rosrun sample_team keyboard_controller.py _name:=zephyr0
  ```

* After that user has control over zephyr and zephyr will be ready to takeoff

![](images/takeoff.png)